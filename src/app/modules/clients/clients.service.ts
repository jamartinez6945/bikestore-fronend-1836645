import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IClient } from './client';
@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IClient[]> {
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients`)
    .pipe(map( res => {
      return res;
    }));
  }// End method query

  /**
   * 1)
   * Implements query String for request to backend
   * @param document 
   */

  public searchClientByDocument(document: string): Observable<IClient[]> {
    let params: HttpParams = new HttpParams();
    params = params.set('document.contains', document);
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients`,{params: params})
    .pipe(map( res => {
      return res;
    }));
  }
}
