import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BikeListComponent } from './bike-list/bike-list.component';
import { BikeCreateComponent } from './bike-create/bike-create.component';
import { BikeUpdateComponent } from './bike-update/bike-update.component';


const routes: Routes = [
  {
    path: 'bike-lists',
    component: BikeListComponent
  },
   {
     path: 'bike-create',
     component: BikeCreateComponent
   },
   {
    path: 'bike-update/:id',
    component: BikeUpdateComponent 
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeRoutingModule { }
