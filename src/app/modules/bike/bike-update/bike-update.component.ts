import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BikeService } from '../bike.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IBike } from '../bike.model';

@Component({
  selector: 'app-bike-update',
  templateUrl: './bike-update.component.html',
  styleUrls: ['./bike-update.component.css']
})
export class BikeUpdateComponent implements OnInit {
  
  formBikeUpdate: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private bikesService: BikeService,
    private formBuilder: FormBuilder
    ) {

      this.formBikeUpdate = this.formBuilder.group({
        id: [],
        model: ['', Validators.required],
        price: [''],
        serial: ['']
      });


     }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID ',id);
    this.bikesService.getById(id)
    .subscribe(res => {
      console.warn('GET DATA ', res);
      this.loadForm(res);
    }, error => {})
  }

  private loadForm(bike: IBike){
    this.formBikeUpdate.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial
    });
  }

  update(): void {
    this.bikesService.update(this.formBikeUpdate.value)
    .subscribe(res => {
      console.warn('UPDATE Ok',res);
      // TODO: Realizar el redireccionamiento a list => Tarea para todos..
    }, err => {
      console.warn('Error ',err);
    });
  }
}
